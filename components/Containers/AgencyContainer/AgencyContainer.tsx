import { StyledAgencyContainer } from "./elements";

export const AgencyContainer = ({ ...props }) => {
  return <StyledAgencyContainer {...props} />;
};