import { StyledTitle } from "@/collections/Card/elements";
import styled from "styled-components";

export const StyledCardContainer = styled(({ topMargin = 0, bottomMargin = 0, ...props }) => <div {...props} />)`
  display: flex;
  background-color: #f7f7f7;
  margin-bottom: 3%;
  padding: 3%;
  border: 3px solid #f7f7f7;
  border-radius: 10px;
  @media (max-width: 1300px) {
    max-width: 65%;
  }
  &:hover {
    border-color: #2196F3;
    ${StyledTitle} {
      color: #2196F3; 
      text-decoration: underline;
    }
  }
  `;