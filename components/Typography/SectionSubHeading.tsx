/* eslint-disable react/display-name */
import { forwardRef } from "react";
import { StyledSectionSubHeading } from "./elements";

export const SectionSubheading = forwardRef(({ ...props }, ref) => {
  return <StyledSectionSubHeading {...props} ref={ref} />;
});