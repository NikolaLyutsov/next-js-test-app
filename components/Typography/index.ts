export * from "./SectionBigHeading";
export * from "./SectionHeading";
export * from "./SectionSubHeading";
export * from "./SectionInnerHeading";
export * from "./SectionParagraph";
export * from "./SectionTinyHeading";
