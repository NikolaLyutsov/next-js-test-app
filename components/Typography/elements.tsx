/* eslint-disable react/display-name */
import { ClassAttributes, ForwardedRef, HTMLAttributes, JSX, forwardRef } from "react";
import styled from "styled-components";

export const StyledSectionBigHeading = styled(forwardRef((props, ref: ForwardedRef<HTMLHeadingElement>) => <h1 {...props} ref={ref} />))`
  font-family: Poppins;
  font-weight: 600;
  font-size: 3rem;
  line-height: 3.94rem;
`;

export const StyledSectionHeading = styled(forwardRef((props, ref: ForwardedRef<HTMLHeadingElement>) => <h2 {...props} ref={ref} />))`
  font-family: Poppins;
  font-size: 2.5rem;
  line-height: 4.375rem;
  font-weight: 600;
`;

export const StyledSectionSubHeading = styled(forwardRef((props, ref: any) => <h3 {...props} ref={ref} />))`
  font-family: Poppins;
  font-size: 1.25rem;
  line-height: 1.875rem;
  font-weight: 300;
`;

export const StyledSectionInnerHeading = styled(forwardRef((props, ref: ForwardedRef<HTMLHeadingElement>) => <h4 {...props} ref={ref} />))`
  font-family: Poppins;
  font-size: 1rem;
  line-height: 1.5rem;
  font-weight: 300;
`;

export const StyledSectionTinyHeading = styled(forwardRef((props, ref: ForwardedRef<HTMLHeadingElement>) => <h5 {...props} ref={ref} />))`
  font-family: Poppins;
  font-size: 0.75rem;
  line-height: 1.125rem;
  font-weight: 300;
`;

export const StyledSectionParagraph = styled((props: JSX.IntrinsicAttributes & ClassAttributes<HTMLParagraphElement> & HTMLAttributes<HTMLParagraphElement>) => <p {...props} />)`
  font-family: Poppins;
  font-size: 1rem;
  line-height: 1.5rem;
  font-weight: 400;
`;
