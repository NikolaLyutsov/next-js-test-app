import { ButtonProps } from "@/types/types";
import { StyledButton, StyledButtonText } from "./elements";

export const Button: React.FC<ButtonProps> = ({ children, ...props }) => {
  return (
    <StyledButton {...props}>
      <StyledButtonText>{children}</StyledButtonText>
    </StyledButton>
  );
};
