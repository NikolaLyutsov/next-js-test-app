import styled from "styled-components";

export const StyledBoldText = styled.div`
  font-size: 28px;
  line-height: 1.2;
  @media (max-width: 1300px) {
    font-size: 24px;
  }
`;
