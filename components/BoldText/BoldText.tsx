import { BoldTextProps } from "@/types/types";
import { StyledBoldText } from "./elements";

export const BoldText = ({ text, target }: BoldTextProps) => {
  const startIndex = text.indexOf(target);
  if (startIndex === -1) return text; 
  const endIndex = startIndex + target.length;
  return (
    <StyledBoldText>
      {text.substring(0, startIndex)}
      <strong>{text.substring(startIndex, endIndex)}</strong>
      {text.substring(endIndex)}
    </StyledBoldText>
  );
};

