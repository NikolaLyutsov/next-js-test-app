import { StyledTitle } from "./elements";
import { BoldText }  from "@/components/BoldText";
import { CardContainer } from "@/components/Containers/CardContainer";
import { CardTextContainer } from "@/components/Containers/CardTextContainer";
import { CardProps } from "@/types/types";

export const Card = ({ title, description, boldText, icon, width }: CardProps) => {
return <CardContainer style={{ width: `${width}px`}}>
          {icon}
          <CardTextContainer>
            <StyledTitle>{title}</StyledTitle>
            <BoldText text={description} target={boldText}/>
          </CardTextContainer>
       </CardContainer>;
};