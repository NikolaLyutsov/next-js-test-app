import { SectionSubheading } from "@/components";
import styled from "styled-components";

export const StyledTitle = styled((props: JSX.IntrinsicAttributes & { [x: string]: any; }) => <SectionSubheading {...props} />)`
  margin: 0;
  color: black;
  font-weight: bold;
  font-size: 32px;
`;
