import React, { ReactNode } from 'react';

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    children: ReactNode;
    variant?: string;
}

export interface ThemeProp { 
    [index: string]: string 
}

export interface ButtonVariantsProps {
    outlined: Array<any>,
    contained: Array<any>,
    text: Array<any>,
    [key: string ]: Array<any> ; 
}

export interface HeroProps {
    image: {
      src: string;
      alt: string;
      width: number;
      height: number;
    };
    title: string;
    description: string;
    ctaText: string;
}

export interface AgencyProps {
    imageVideo: { src: string, alt: string },
    title: string,
    description: string,
    cardBriefTitle: string,
    cardBriefDescription: string,
    cardBriefBoldText: string,
    cardBriefIcon: object,
    cardBriefWidth: number,
    cardSearchTitle: string,
    cardSearchDescription: string,
    cardSearchBoldText: string,
    cardSearchIcon: object,
    cardSearchWidth: number,
    cardPitchTitle: string,
    cardPitchDescription: string,
    cardPitchBoldText: string,
    cardPitchIcon: object,
    cardPitchWidth: number
};

export interface BoldTextProps { 
    text: string,
    target: string 
}
export interface CardProps {
    title: string,
    description: string,
    boldText: string, 
    icon: any, 
    width: number
}