/* eslint-disable @next/next/no-page-custom-font */
import { FaHandsHelping, FaSearch } from "react-icons/fa";
import { Hero, Agency } from "../sections";
import Head from 'next/head';
import { BsFillPeopleFill } from "react-icons/bs";

const heroProps = {
  image: { src: "/img/heroimg.png", alt: "", width: 1000, height: 1680 },
  title: "Agency procurement, outsourced.",
  description: "Start the process here",
  ctaText: "Start",
};

const agencyProps = {
  imageVideo: { src: "/img/video.png", alt: "videoImage" },
  title: "Managed agency selection",
  description: "Strengthen your onboarding process",
  cardBriefIcon: <FaHandsHelping size={130}/>,
  cardBriefWidth: 550,
  cardBriefTitle: "Brief",
  cardBriefDescription: "Complete brief writing or simple guidance on what to include, we`ve got you covered.",
  cardBriefBoldText: "brief writing or simple guidance",
  cardSearchIcon: <FaSearch  size={130}/>,
  cardSearchWidth: 590,
  cardSearchTitle: "Search",
  cardSearchDescription: "In-depth agency search covering; criteria matching, door knocking and due-diligence vetting.",
  cardSearchBoldText: "criteria matching",
  cardPitchIcon: <BsFillPeopleFill  size={130}/>,
  cardPitchWidth: 630,
  cardPitchTitle: "Pitch",
  cardPitchDescription: "Comprehensive pitch management, including comms, diary management and pitch hosting.",
  cardPitchBoldText: "pitch management"
};

export default function Home() {
  return (
    <>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          rel="preconnect"
          href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        />
      </Head>
      <div>
        {/* <Hero {...heroProps} /> */}
        <Agency {...agencyProps}/>
      </div>
    </>
  );
}
