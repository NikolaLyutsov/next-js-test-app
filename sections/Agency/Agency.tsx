import { AgencyProps } from "@/types/types";
import Image from "next/legacy/image";
import { StyledAgencyCardContainer, StyledAgencyContainer, StyledAgencyDescription, StyledAgencyImageContainer, StyledAgencyTextContainer, StyledAgencyTitle, StyledBackgroundImageContainer } from "./elements";
import { Card } from "@/collections";

export const Agency: React.FC<AgencyProps> = ({ imageVideo, title, 
     description, cardBriefTitle, cardBriefDescription, cardBriefBoldText, cardBriefIcon, cardBriefWidth, cardSearchTitle, cardSearchDescription,
     cardSearchBoldText, cardSearchIcon, cardSearchWidth, cardPitchTitle, cardPitchDescription, cardPitchBoldText, cardPitchIcon, cardPitchWidth }) => {
    return (
        <StyledAgencyContainer>
            <StyledAgencyTextContainer>
                <StyledAgencyTitle>{title}</StyledAgencyTitle>
                <StyledAgencyDescription>{description}</StyledAgencyDescription>
            </StyledAgencyTextContainer>
        <StyledBackgroundImageContainer>
            <StyledAgencyImageContainer>
            <Image src={imageVideo.src} alt={imageVideo.alt}  layout="fill" />
            </StyledAgencyImageContainer>
            <StyledAgencyCardContainer>
                <Card title={cardBriefTitle} description={cardBriefDescription} boldText={cardBriefBoldText} icon={cardBriefIcon} width={cardBriefWidth}/>
                <Card title={cardSearchTitle} description={cardSearchDescription} boldText={cardSearchBoldText} icon={cardSearchIcon} width={cardSearchWidth}/>
                <Card title={cardPitchTitle} description={cardPitchDescription} boldText={cardPitchBoldText} icon={cardPitchIcon} width={cardPitchWidth}/>
            </StyledAgencyCardContainer>
        </StyledBackgroundImageContainer>
        </StyledAgencyContainer>
    );
  };