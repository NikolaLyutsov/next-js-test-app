import { SectionBigHeading, SectionSubheading } from "@/components";
import { AgencyContainer } from "@/components/Containers/AgencyContainer/AgencyContainer";
import { RefAttributes } from "react";
import { styled } from "styled-components";

export const StyledAgencyContainer = styled(({ height, ...props }: any) => <AgencyContainer {...props} />)`
  flex-direction: column;
  font-family: sans-serif;
`;
export const StyledAgencyTextContainer = styled(({ ...props }) => <div {...props} />)`
  display: flex;
  align-items: center;
  flex-direction: column;
  font-family: sans-serif;
  margin: 5% 0% 5% 0%;
`;
export const StyledAgencyTitle = styled((props: JSX.IntrinsicAttributes & { [x: string]: any; }) => <SectionBigHeading {...props} />)`
  margin: 0;
  font-size: 4rem;
  color: black;
`;
export const StyledAgencyDescription = styled((props: (JSX.IntrinsicAttributes & RefAttributes<unknown>) | object) => <SectionSubheading {...props} />)`
  margin: 1.563rem 0 0;
  font-size: 2rem;
`;
export const StyledBackgroundImageContainer = styled(({ ...props }) => <div {...props} />)`
  background-image: url('/img/background.png');
  display: flex;
  max-width: 129.00rem;
  background-repeat: no-repeat;
  background-size: 1270px;
  background-position: 0 50px;
  width: 100%;
  @media (max-width: 1024px) {
    flex-direction: column;
    background-position: 30% 50px;
  }
`;
export const StyledAgencyImageContainer = styled(({ ...props }) => <div {...props} />)`
  position: relative;
  width: 820px;
  height: 720px;
  margin-left: 17%;
  margin-bottom: 5%;
  @media (max-width: 1300px) {
    min-width: 320px;
    height: 660px;
    margin-left: 24%;
  }
  @media (max-width: 1024px) {
    width: 420px;
    height: 710px;
    margin-left: 24%;
  }
`;
export const StyledAgencyCardContainer: any = styled(({ ...props }) => <div {...props} />)`
  width: 100%;
  height: 100%;
  margin-top: 30px;
  margin-left: 5%;
  @media (max-width: 1024px) {
    margin-left: 14%;
  }
`;